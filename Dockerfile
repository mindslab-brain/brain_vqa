FROM docker.maum.ai:443/brain/vision:v0.2.0-cu101-torch160

COPY . /app
WORKDIR /app

EXPOSE 5959

RUN pip install grpcio
RUN pip install grpcio-tools
RUN pip install -r requirements.txt

RUN bash build_proto.sh
RUN cd /app/src/models/vlt5 && python download_backbones.py
