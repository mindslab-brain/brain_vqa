# Brain_VQA

Author: Sungguk Cha, Jinwoo Kim

# Usage

## 0. Download pretrained model
```angular2html
download checkpoint
   /DATA2/vision/checkpoints/eyes/vqa/vlt5/VLT5_longvqa_10.pth
put
   /app/weight/VLT5_checkpoint.pth
```

## 1. Docker
### Load Docker Image
```
    docker pull docker.maum.ai:443/brain/brain_vqa:v1.0.2
```
### Load Docker Server Image
```
    docker pull docker.maum.ai:443/brain/brain_vqa:v1.0.2-server
```
### Run docker-server
```
    bash docker_run.sh
```

## 2. Run server

### 2-1. Lxmert (VQA)
```
    cd /app/src
    python server.py --config /app/conf/lxmert_config.yaml
```

### 2-2. Vlt5 (GQA)
```
    cd /app/src
    python server.py --config /app/conf/vlt5_config.yaml
```

## 3. Run client
```
    cd /app/src
    python client.py
```
