GPUS="0"
DOCKER_IMAGE="docker.maum.ai:443/brain/brain_vqa:v1.0.2-server"

echo "Run docker using GPU(s): ${GPUS}"

docker run -itd \
--name brain_vqa \
--gpus "\"device=${GPUS}\"" \
--ipc=host \
"${DOCKER_IMAGE}" bash;
