r'''
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
    reference: https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_stylebot_wild_segmentation/browse?at=refs%2Fheads%2Fhksong-review
'''

from concurrent import futures
import sys
import time
import argparse

from models import get_inference_function
from models.lxmert.utils import setup_logger
sys.path.append("..")
from proto.vqa_pb2 import VQAOutput
from proto.vqa_pb2_grpc import VQAServicer, add_VQAServicer_to_server

import cv2
import grpc
import numpy as np
from omegaconf import OmegaConf

ONE_DAY_IN_SECONDS = 60 * 60 * 24
CHUNK_SIZE = 1024 * 1024
MAX_SIZE = 4 * CHUNK_SIZE


class VQAServicerImpl(VQAServicer):
    def __init__(self, configs, logger):
        self.models = get_inference_function(configs)
        self.logger = logger

    def Determinate(self, vqa_iterator, context):
        try:
            image_data = bytearray()
            text_data = ""

            for input_data in vqa_iterator:
                if input_data.image is not None and len(input_data.image) != 0:
                    image_data.extend(input_data.image)
                if input_data.text is not None and len(input_data.text) != 0:
                    text_data = input_data.text
                if len(image_data) > MAX_SIZE or len(bytes(input_data.text, encoding='utf-8')) > CHUNK_SIZE:
                    err = "input size error: image < 4MB, text < 1MB"
                    self.logger.debug("input size error: image < 4MB, text < 1MB")
                    return self._generate_binary_iterator(err)

            image_data = bytes(image_data)
            self.logger.info(f'request:{len(image_data)} & {len(bytes(text_data, encoding="utf-8"))} bytes')

            image_np = cv2.imdecode(np.frombuffer(image_data, dtype=np.uint8), cv2.IMREAD_COLOR)
            questions = text_data.split(',')

            before = time.time()
            results = self.models.inference(image_np, questions)
            self.logger.debug(f'inference_time: {time.time() - before}')

            ans = ""
            for i, res in enumerate(results):
                if i > 0: ans += ", "
                ans += res

            return self._generate_binary_iterator(ans)

        except AssertionError as e:
            self.logger.exception(e)
            context.set_code(grpc.StatusCode.UNIMPLEMENTED)
            context.set_details(str(e))

        except Exception as e:
            self.logger.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    @staticmethod
    def _generate_binary_iterator(binary):
        return VQAOutput(text=binary)

def main():
    parser = argparse.ArgumentParser(description='VQA Server')
    parser.add_argument('--config',
                        type=str,
                        default="../conf/lxmert_config.yaml")
    parser.add_argument('--log_level',
                        type=str,
                        help='logger level (default: INFO)',
                        default='INFO')
    args = parser.parse_args()

    logger = setup_logger(logger_name='Brain_VQA', level=args.log_level.upper())
    config = OmegaConf.load(args.config)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    service_server = VQAServicerImpl(config, logger)
    add_VQAServicer_to_server(service_server, server)
    server.add_insecure_port(f"[::]:{config.port}")
    server.start()

    logger.info("server started >> {}".format(config.port))
    try:
        while True:
            time.sleep(ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    main()
