r'''
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
'''

import sys
import time

sys.path.append("..")

from proto.vqa_pb2 import VQAInput
from proto.vqa_pb2_grpc import VQAStub

import grpc
import argparse

CHUNK_SIZE = 1024 * 1024  # 1MB


def list2string(qs):
    ans = qs[0]
    for i in range(1, len(qs)):
        ans += ", " + qs[i]
    return ans

class Client(object):
    def __init__(self, ip="localhost", port=5959):
        self.server_ip = ip
        self.server_port = port
        self.stub = VQAStub(
            grpc.insecure_channel(
                self.server_ip + ":" + str(self.server_port)
            )
        )

    def get_service(self, image_path, questions):
        with open(image_path, 'rb') as rf:
            image_byte = rf.read()
        text = list2string(questions)

        binary_iterator = self._generate_binary_iterator(image_byte, text)

        before = time.time()
        output = self.stub.Determinate(binary_iterator)
        after = time.time()
        passed = after - before

        text = output.text
        answers = text.split(',')

        for i, q in enumerate(questions):
            print(q, ":",  answers[i])
        print(passed, "second passed")

    @staticmethod
    def _generate_binary_iterator(image_binary, text):
        for idx in range(0, len(image_binary), CHUNK_SIZE):
            yield VQAInput(image=image_binary[idx:idx+CHUNK_SIZE])
        yield VQAInput(text=text)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='VQA Client')
    parser.add_argument('--ip',
                        type=str,
                        help="VQA server ip (default: localhost)",
                        default="0.0.0.0")
    parser.add_argument('--port',
                        type=int,
                        default=5959)
    args = parser.parse_args()

    image_path = "../sample/sample.jpg"
    text = ["Which vegetable is there?", "Which vegetable is there?", "What the person is doing?"]

    client = Client(ip=args.ip, port=args.port)
    client.get_service(image_path, text)
