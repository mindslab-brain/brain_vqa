import torch

from . import utils
from .modeling_frcnn import GeneralizedRCNN
from .processing_image import Preprocess

from transformers import LxmertForQuestionAnswering, LxmertTokenizer


class Lxmert:
    def __init__(self, configs):
        VQA_URL = "https://raw.githubusercontent.com/airsplay/lxmert/master/data/vqa/trainval_label2ans.json"
        self.device = torch.device('cuda:{}'.format(configs.gpu) if torch.cuda.is_available() else 'cpu')

        self.frcnn_cfg = utils.Config.from_pretrained("unc-nlp/frcnn-vg-finetuned")
        self.frcnn_cfg.model.device = self.device

        self.vqa_answers = utils.get_data(VQA_URL)
        self.image_preprocess = Preprocess(self.frcnn_cfg)

        self.frcnn = GeneralizedRCNN.from_pretrained("unc-nlp/frcnn-vg-finetuned", config=self.frcnn_cfg)
        self.lxmert_tokenizer = LxmertTokenizer.from_pretrained("unc-nlp/lxmert-base-uncased")
        self.lxmert_vqa = LxmertForQuestionAnswering.from_pretrained("unc-nlp/lxmert-vqa-uncased")
        self.lxmert_vqa = self.lxmert_vqa.to(self.device)
        self.lxmert_vqa.eval()

    def inference(self, image, questions):
        with torch.no_grad():
            images, sizes,  scales_yx = self.image_preprocess(image)
            images = images.to(self.device)
            output_dict = self.frcnn(
                images,
                sizes,
                scales_yx=scales_yx,
                padding='max_detections',
                max_detections=self.frcnn_cfg.max_detections,
                return_tensors='pt'
            )

            normalized_boxes = output_dict.get("normalized_boxes")
            features = output_dict.get("roi_features")

            inputs = self.lxmert_tokenizer(
                questions,
                padding="max_length",
                max_length=20,
                truncation=True,
                return_token_type_ids=True,
                return_attention_mask=True,
                add_special_tokens=True,
                return_tensors="pt"
            )
            inputs = inputs.to(self.device)
            normalized_boxes = normalized_boxes.to(self.device)
            features = features.to(self.device)

            output_vqa = self.lxmert_vqa(
                input_ids=inputs.input_ids,
                attention_mask=inputs.attention_mask,
                visual_feats=features,
                visual_pos=normalized_boxes,
                token_type_ids=inputs.token_type_ids,
                return_dict=True,
                output_attentions=False,
            )

            pred_vqa = output_vqa["question_answering_score"].argmax(-1)
            results = list()
            for pred in pred_vqa:
                results.append(self.vqa_answers[pred])
            return results
