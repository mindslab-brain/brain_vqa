import os
import sys
import torch

sys.path.append(os.path.dirname(__file__))
from vlt5_src.src.param import parse_args
from vlt5_src.src.vqa import Trainer
from vlt5_src.src.tokenization import VLT5Tokenizer
from vlt5_src.inference.processing_image import Preprocess
from vlt5_src.inference.modeling_frcnn import GeneralizedRCNN
from vlt5_src.inference.utils import Config, get_data


class Vlt5:
    def __init__(self, configs):
        GQA_URL = "https://raw.githubusercontent.com/airsplay/lxmert/master/data/gqa/trainval_label2ans.json"
        self.device = torch.device('cuda:{}'.format(configs.gpu) if torch.cuda.is_available() else 'cpu')

        self.frcnn_cfg = Config.from_pretrained("unc-nlp/frcnn-vg-finetuned")
        self.frcnn_cfg.model.device = self.device

        self.gqa_answers = get_data(GQA_URL)
        self.image_preprocess = Preprocess(self.frcnn_cfg)
        args = parse_args(  # custom parse_args
            parse=configs.parse,
            backbone=configs.backbone,
            load=configs.load,
            gpu=self.device,
        )

        self.frcnn = GeneralizedRCNN.from_pretrained("unc-nlp/frcnn-vg-finetuned", config=self.frcnn_cfg)
        self.tokenizer = VLT5Tokenizer.from_pretrained("t5-base")
        self.vlt5 = Trainer(args, train=False)
        self.vlt5.model.eval()

    def inference(self, image, questions):

        with torch.no_grad():
            images, sizes, scales_yx = self.image_preprocess(image)
            output_dict = self.frcnn(
                images,
                sizes,
                scales_yx=scales_yx,
                padding='max_detections',
                max_detections=self.frcnn_cfg.max_detections,
                return_tensors='pt'
            )

            normalized_boxes = output_dict.get("normalized_boxes").to(self.device)
            features = output_dict.get("roi_features").to(self.device)

            # TODO 진짜 batch화는 안되는가? -> 테스트해보기
            result = []
            for question in questions:
                batch = {}

                input_ids = self.tokenizer(question, return_tensors='pt', padding=True).input_ids
                input_ids = input_ids.to(self.device)

                batch['input_ids'] = input_ids
                batch['vis_feats'] = features
                batch['boxes'] = normalized_boxes

                pred = self.vlt5.model.test_step(batch)
                result.append(pred['pred_ans'][0])

            return result
