from models.lxmert.lxmert_inference import Lxmert
from models.vlt5.vlt5_inference import Vlt5

def get_inference_function(config):
    if config.model == "lxmert":
        print("get Lxmert model")
        return Lxmert(config)
    elif config.model == "vlt5":
        print("get Vlt5 generative model")
        return Vlt5(config)
    else:
        return None
